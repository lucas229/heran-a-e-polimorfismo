#ifndef TRIANGULO_HPP
#define TRIANGULO_HPP

#include "formageometrica.hpp"

class Triangulo: public FormaGeometrica {
public:
    Triangulo(float base, float altura);
    ~Triangulo();
    float calcula_area();
    float calcula_perimetro();
};

#endif