#ifndef PARALELOGRAMO_HPP
#define PARALELOGRAMO_HPP

#include "formageometrica.hpp"

class Paralelogramo: public FormaGeometrica {
private:
    float lado;
public:
    Paralelogramo(float base, float lado, float altura);
    ~Paralelogramo();
    void set_lado (float lado);
    float get_lado ();
    float calcula_perimetro();
};

#endif