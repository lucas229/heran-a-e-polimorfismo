#ifndef CIRCULO_HPP
#define CIRCULO_HPP

#include "formageometrica.hpp"

class Circulo: public FormaGeometrica {
private:
    float raio;
public:
    Circulo (float raio);
    ~Circulo();
    void set_raio(float raio);
    float get_raio();
    float calcula_area();
    float calcula_perimetro();
};

#endif