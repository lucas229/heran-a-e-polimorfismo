#ifndef QUADRADO_HPP
#define QUADRADO_HPP

#include "formageometrica.hpp"

class Quadrado: public FormaGeometrica {
public:
    Quadrado(float base);
    ~Quadrado();
    float calcula_area();
    float calcula_perimetro();
};

#endif