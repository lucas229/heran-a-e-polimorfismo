#ifndef HEXAGONO_HPP
#define HEXAGONO_HPP

#include "formageometrica.hpp"

class Hexagono: public FormaGeometrica {
public:
    Hexagono(float base);
    ~Hexagono();
    float calcula_area();
    float calcula_perimetro();
};

#endif