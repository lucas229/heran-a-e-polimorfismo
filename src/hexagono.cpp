#include "hexagono.hpp"

#include <iostream>
#include <math.h>

Hexagono::Hexagono(float base) {
    set_tipo ("Hexágono");
    set_base(base);
}

float Hexagono::calcula_area() {
    return 3*get_base()*get_base()*sqrt(3)/2;
}

float Hexagono::calcula_perimetro() {
    return 6*get_base();
}