#include "quadrado.hpp"

#include <iostream>

Quadrado::Quadrado(float base) {
    set_tipo ("Quadrado");
    set_base(base);
}

Quadrado::~Quadrado() {
}

float Quadrado::calcula_area() {
    return get_base()*get_base();
}

float Quadrado::calcula_perimetro() {
    return 4*get_base();
}