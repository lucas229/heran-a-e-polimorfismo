#include "formageometrica.hpp"
#include "triangulo.hpp"
#include "quadrado.hpp"
#include "circulo.hpp"
#include "paralelogramo.hpp"
#include "pentagono.hpp"
#include "hexagono.hpp"

#include <iostream>
#include <vector>

int main () {
    vector <FormaGeometrica*> formas;
    formas.push_back(new Triangulo(5, 3)); //base, altura
    formas.push_back(new Quadrado(6)); //lado
    formas.push_back(new Circulo(2)); //raio
    formas.push_back(new Paralelogramo(4, 9, 12)); //lado 1, lado 2, altura
    formas.push_back(new Pentagono(16)); //lado
    formas.push_back(new Hexagono(9)); //lado
    for (FormaGeometrica *f: formas) {
        f->imprime_dados();
    }
    return 0;
}