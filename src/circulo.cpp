#include "circulo.hpp"

#include <iostream>

Circulo::Circulo (float raio) {
    set_tipo("Círculo");
    set_raio(raio);
}

Circulo::~Circulo() {
}

void Circulo::set_raio(float raio) {
    this->raio=raio;
}

float Circulo::get_raio() {
    return raio;
}

float Circulo::calcula_area() {
    return 3.14159265359*get_raio()*get_raio();
}

float Circulo::calcula_perimetro() {
    return 2*3.14159265359*get_raio();
}