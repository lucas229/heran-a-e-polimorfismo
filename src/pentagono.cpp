#include "pentagono.hpp"

#include <iostream>
#include <math.h>

Pentagono::Pentagono(float base) {
    set_tipo("Pentágono");
    set_base(base);
}

Pentagono::~Pentagono(){
}

float Pentagono::calcula_area() {
    return sqrt(25+10*sqrt(5))/4*get_base()*get_base();
}

float Pentagono::calcula_perimetro() {
    return 5*get_base();
}