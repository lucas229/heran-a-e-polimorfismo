#include "paralelogramo.hpp"

#include <iostream>

Paralelogramo::Paralelogramo(float base, float lado, float altura) {
    set_tipo("Paralelogramo");
    set_base(base);
    set_lado(lado);
    set_altura(altura);
}

Paralelogramo::~Paralelogramo() {
}

void Paralelogramo::set_lado (float lado) {
    this->lado=lado;
}

float Paralelogramo::get_lado () {
    return lado;
}

float Paralelogramo::calcula_perimetro() {
    return 2*(get_base()+get_lado());
}